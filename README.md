# GIT

Tema pentru trainingul de GIT.

## Comenzi

```
git clone https://Arvinte@bitbucket.org/Arvinte/basics.git
cd basics/
mkdir bash
mkdir python
cd bash
vim basic.sh
cd ..
cd python/
vim basic.py
vim modul.py
cd ..
git add ./bash/basic.sh
git add ./python/basic.py
git add ./python/modul.py
git commit -m "First commit"
vim README.md
git add README.md
git commit -m "Second commit"
git log
git push origin master
```